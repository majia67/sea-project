# README #

This package is the final project for New York University's Search Engine Architecture class.

Author: Yicong Tao (yt1182@nyu.edu), Jinglin Wang (jw4716@nyu.edu)

### Prerequisites ###

+ python: >=2.7.13
+ tornado: >=4.5.1
+ python-midi: >=0.2.4

### Installation ###
Simply clone this repository using Git.
```
git clone https://majia67@bitbucket.org/majia67/sea-project.git
```
We assume your working directory is *sea-project/music_plagiarism* when you are running this program.

### Configuration ###
Before building index, you need to set some basic configurations (though you may choose the default one).

common/inventory.py
This is where you can set configurations that applied to the whole package, including:
  + IDX\_SHARD\_NUM: number of index servers
  + MIDI\_SHARD\_NUM: number of midi servers
  + IDX\_SHARD\_PATH: path to index store
  + MIDI\_SHARD\_PATH: path to midi store
  + SLIDING\_WINDOW\_SIZE: size of the sliding window (WS) your index has. Note this number should be consistent with the sliding window size you use when making index.

indexer/inventory.py
This is where you configure indexer subpackage, including:
  + midifile\_path: path to the midi files you want to index
  + midi\_job\_path: path to temporary storage folder putting files when running indexer
  + SLIDING\_WINDOW\_SIZE: size of the sliding window (WS) your index will have

search/inventory.py
This is where you configure the search engine, including:
  + SCORE\_MATCH, SCORE\_MISMATCH, SCORE\_SN_MATCH: different weight for perfect match/mismatch/standard note match
  + several server configurations

If there's no songlist.txt file under 'common' folder, make an empty one.

### Building Index - Simple Way ###
We provide a one-click script that could run the whole indexing process. Make sure you properly configured the inventory files.
```
python -m indexer.midi_transform
```
This method is suitable for building small indexes (such as the text files we provided).

### Building Index - Harder Way ###
We also provide a script that could run the whole indexing process for building large indexes. Make sure you properly configured the inventory files.

Open two terminals. In the first terminal, execute:
```
python -m mapreduce.workers
```
In the second terminal, execute:
```
python -m indexer.midi_start
```
The job should automatically start.

### Building Index - Manual Way ###
In case you need the most flexibility, you could build the index by running all scripts step by step.

#### Reformat your data ####
First, you need to reformat your MIDI files by assigning them unique midiID and generating midi note sequence store. This will also generate a key-value pair data suitable for building indexes using MapReduce. The generated midi store is located in common/data/midi folder.

To run reformatter, you need to provide three arguments. 'midi\_folder\_path' is where to put your original MIDI files that should be reformatted. 'midi\_jobs\_path'
```
python -m indexer.midi_reformatter --new_folder=midi_folder_path --job_path=midi_jobs_path --num_partitions=MIDI_SHARD_NUM
```

#### Building index ####
Then we use Hadoop MapReduce to build index files which are used by the search engine.
Open two terminals. In the first terminal, execute:
```
python -m mapreduce.workers
```
In the second terminal, execute:
```
python -m mapreduce.coordinator --mapper_path=indexer/midi_mapper.py --reducer_path=indexer/midi_reducer.py --job_path=midi_job_path, --num_reducers=IDX_SHARD_NUM
```

#### Merge index into database ####
After succesfully building the index, you can now safely merge them into index databases which may have existed index files.
```
python -m indexer.moveOutFiles
```

### How to start the search engine ###
```
python -m search.start
```
Then in the browser, type
```
http://localhost:13160
```
You should see the search engine interface.
