"""Inventory module for the package

Please put configuration that applies to the whole package here
"""

# Data store
IDX_SHARD_NUM = 4
MIDI_SHARD_NUM = 4
IDX_SHARD_PATH = 'common/data/index'
MIDI_SHARD_PATH = 'common/data/midi'
songlist_path = "common/songlist.txt"

# MIDI-related
MIDI_NOTE_NUMBER = 128
MIDI_OCTAVE_SIZE = 12
MIDI_STD_NOTE_BITS = 4

# Sliding Window
SLIDING_WINDOW_SIZE = 5
