import midi

pattern = midi.read_midifile("mary.mid")
on =[]
off = []

for track in pattern:
    for i in range(len(track)):
        event = track[i]
        if event.name == "Note On":
            v = event.data[1]
            if v != 0:
                on.append(event)
            if v == 0:
                off.append(event)
        elif event.name == "Note Off":
            off.append(event)

pattern = midi.Pattern()
# Instantiate a MIDI Track (contains a list of MIDI events)
track = midi.Track()
# Append the track to the pattern
pattern.append(track)

track.extend(on)
track.extend(off)
print pattern
midi.write_midifile("example.mid", pattern)