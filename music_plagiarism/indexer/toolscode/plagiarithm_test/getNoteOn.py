import midi
import argparse


def getNoteOn(midi_file):
    pattern = midi.read_midifile(midi_file)
    tracks = []
    info = []
    for track in pattern:
        channel = {}
        for event in track:
            if event.name == "Note On":
                if event.data[1] != 0:
                    if event.channel in channel:
                        channel[event.channel].append(event.data[0])
                    else:
                        channel[event.channel] = [event.data[0]]
        tracks.append(channel)
    for i,c in enumerate(tracks):
        for key, value in c.items():
            info.append({"track":i, "channel": key, "notes": value})
    return info


argparser = argparse.ArgumentParser()
argparser.add_argument("filepath", help = "input file path")
args = argparser.parse_args()

info = getNoteOn(args.filepath)
for item in info:
    print('%d-%d: %s' % (item['track'], item['channel'], ' '.join([str(i) for i in item['notes']])))

