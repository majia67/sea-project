from music21 import converter, instrument
file = converter.parse("shapeofyou.mid")
s2 = instrument.partitionByInstrument(file)
for i, part in enumerate(s2.parts):
    fp = part.write('midi', 'split{}.midi'.format(i))
    print(fp)