import midi

original_pattern = midi.read_midifile("mary.mid")
patterns = []
channels = []
prefix = []
count = 0

general_names = []
event_names = []
for track in original_pattern:
    for i in range(len(track)):
        event = track[i]
        try:
            if event.channel in channels:
                idx = channels.index(event.channel)
                if event.name not in event_names[idx]:
                    event_names[idx].append(event.name)
            else:
                channels.append(event.channel)
                event_names.append([event.name])
        except AttributeError:
            if event.name not in general_names:
                general_names.append(event.name)

print general_names
for i in range(len(event_names)):
    print ("channel"+str(channels[i]), event_names[i])