import midi

original_pattern = midi.read_midifile("mary.mid")
patterns = []
channels = []
prefix = []
count = 0

general_names = ['Track Name', 'Copyright Notice', 'Set Tempo', 'Time Signature', 'Lyrics', 'End of Track']
event_names = ['SysEx', 'Control Change', 'Program Change', 'Pitch Wheel', 'Note On', 'Note Off']
note_names = ['Note On', 'Note Off']
for track in original_pattern:
    for i in range(len(track)):
        event = track[i]
        if event.name in general_names:
            prefix.append(event)
        elif event.name in note_names:
            if event.channel in channels:
                idx = channels.index(event.channel)
                patterns[idx][-1].append(event)
            else:
                #print(event.channel)
                new_pattern = midi.Pattern()
                new_track = midi.Track()
                new_track.append(event)
                new_pattern.append(new_track)
                patterns.append(new_pattern)
                channels.append(event.channel)

eot = midi.EndOfTrackEvent(tick=1)

for i in range(len(patterns)):
    for track in patterns[i]:
        #track = prefix + track
        track.append(eot)
    midi.write_midifile("example"+str(i)+".mid", patterns[i])
