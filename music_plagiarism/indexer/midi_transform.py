import midi
import os
import sys
import argparse
import pickle
from indexer.inventory import note_name, midifile_path
from common.inventory import SLIDING_WINDOW_SIZE, IDX_SHARD_PATH, MIDI_SHARD_PATH, IDX_SHARD_NUM, MIDI_SHARD_NUM, MIDI_OCTAVE_SIZE, songlist_path
from search.blast import get_sliding_windows

# Function used to split channels in midi file

def getNoteOn(midi_file):
    pattern = midi.read_midifile(midi_file)
    split_channels(pattern)

def split_channels(pattern):
    tracks = []
    info = []
    for track in pattern:
        channel = {}
        for event in track:
            if event.name in note_name:
                if event.data[1] != 0:
                    if event.channel in channel:
                        channel[event.channel].append(event.data[0])
                    else:
                        channel[event.channel] = [event.data[0]]
        tracks.append(channel)
    for i, c in enumerate(tracks):
        for key, value in c.items():
            info.append({"track":i, "channel": key, "notes": value})
    return info


def main():

    # Check and create shard folders
    for i in range(MIDI_SHARD_NUM):
        if not os.path.exists(MIDI_SHARD_PATH+"/"+str(i)):
            os.makedirs(MIDI_SHARD_PATH+"/"+str(i))
    for i in range(IDX_SHARD_NUM):
        if not os.path.exists(IDX_SHARD_PATH+"/"+str(i)):
            os.makedirs(IDX_SHARD_PATH+"/"+str(i))

    midi_id = 0
    if os.path.isfile(songlist_path):
        songlist = open(songlist_path, 'r+')
        # Get the last line of the songlist(song_id\tsong_title)
        last_line = ''
        for line in songlist:
            last_line = line

        # Compute the starting midi_id of this file. If the file is empty, assign midi_id to 0
        if last_line:
            midi_id = int(line.split('\t')[0]) + 1
    else:
        songlist = open(songlist_path, 'w')

    index = {}
    count = 0
    new_folder_path = midifile_path

    # For every midi file in the newly added foler
    for path, subdirs, midifile in os.walk(new_folder_path):
        for file_name in midifile:
            # If the file is not a midi file (not end with .mid)
            if file_name[-4:] != ".mid":
                continue

            try:
                pattern = midi.read_midifile(os.path.join(path, file_name))
            except Exception, e:
                print(str(e))
                continue

            # Split different channels
            #tracks, channels = split_channels(pattern)
            info = split_channels(pattern)

            for i in info:
                note_seq = i["notes"]

                # Only consider channels with note sequence size larger than window size
                if len(note_seq) >= SLIDING_WINDOW_SIZE:
                    # Generate sliding windows
                    window_list = get_sliding_windows(note_seq)

                    for j, midi_segment in enumerate(window_list):
                        # Write midi_segment to index with format (midi_id, position)
                        if midi_segment in index:
                            index[midi_segment].append((midi_id, j))
                        else:
                            index[midi_segment] = [(midi_id, j)]

                    # Write midi id, note sequence and song title to the doc dictionary
                    midi_shard = midi_id % MIDI_SHARD_NUM
                    midi_store = {"notes":note_seq, "title": file_name, "channel": i["channel"], "track": i["track"]}
                    pickle.dump(midi_store, open("%s/%d/%d" % (MIDI_SHARD_PATH, midi_shard, midi_id), "wb"))
                    songlist.write("%d\t%s\n" % (midi_id, file_name))
                    midi_id += 1

            # Print log information
            print("Finished processing: %s" % (file_name))
            count += 1
            if count % 100 == 0:
                print("Number of songs completed: %d" % (count))
                sys.stdout.flush()

    songlist.close()

    # Write index into files
    for midi_segment in index:
        idx_shard = midi_segment % IDX_SHARD_NUM
        pickle.dump(index[midi_segment], open("%s/%d/%d" % (IDX_SHARD_PATH, idx_shard, midi_segment), "wb"))


if __name__ == '__main__':
    main()
