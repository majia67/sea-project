"""Inventory module for indexer part

Please put configuration that applies to the indexer here
"""

note_name = ['Note On']
midifile_path = "indexer/data/test"
midi_job_path = "indexer/midi_jobs"

SLIDING_WINDOW_SIZE = 5
MIDI_STD_NOTE_BITS = 4
