from indexer.inventory import midi_job_path
from common.inventory import IDX_SHARD_PATH, IDX_SHARD_NUM
import os
import pickle
import json

index_path = []
for i in range(IDX_SHARD_NUM):
    index_path.append('%s/%d' % (IDX_SHARD_PATH, i))
    if not os.path.exists(index_path[i]):
        os.makedirs(index_path[i])

for file_name in os.listdir(midi_job_path):
    if file_name[-4:] != ".out":
        continue
    infile = open(midi_job_path + "/" + file_name, "r")
    for line in infile:
        midi_segment, index_list = line.split('\t')
        index_list = [tuple(item) for item in json.loads(index_list)]
        idx_shard = int(midi_segment) % IDX_SHARD_NUM
        file_path = '%s/%s' % (index_path[idx_shard], midi_segment)
        if os.path.isfile(file_path):
            sub_index = pickle.load(open(file_path, "rb"))
            index_list.extend(sub_index)
        #print(index_list)
        pickle.dump(index_list, open(file_path, "wb"))
