#!/usr/bin/env python
import sys, json, math, pickle

count = 0
key, json_dump = sys.stdin.readline().strip().split("\t")
index_list = [json.loads(json_dump)]

for line in sys.stdin:
    midi_segment, json_dump = line.strip().split("\t")
    if key != midi_segment:
        print('%s\t%s' % (key, json.dumps(index_list)))
        key = midi_segment
        index_list = []

        count += 1
        if count % 1000 == 0:
            sys.stderr.write("reducer: " + str(count) + "\n")
    index_list.append(json.loads(json_dump))

print('%s\t%s' % (key, json.dumps(index_list)))

# for midi_segment, index_list in index.items():
#     print('%s\t%s' % (midi_segment, json.dumps(index_list)))
# pickle.dump(index, sys.stdout)