#!/usr/bin/env python
import sys, json
from inventory import SLIDING_WINDOW_SIZE, MIDI_STD_NOTE_BITS

window_filter = int('1' * (SLIDING_WINDOW_SIZE * MIDI_STD_NOTE_BITS), 2)
count = 0
for line in sys.stdin:
    midi_id, note_seq = line.strip().split('\t')
    midi_id = int(midi_id)
    std_note_list = note_seq.strip().split()

    # Generate first window
    window = 0
    for i in range(SLIDING_WINDOW_SIZE):
        window <<= MIDI_STD_NOTE_BITS
        window |= int(std_note_list[i])
    print("%d\t%s" % (window, json.dumps((midi_id, 0))))

    # Generate windows
    for i in range(SLIDING_WINDOW_SIZE, len(std_note_list)):
        window <<= MIDI_STD_NOTE_BITS
        window &= window_filter
        window |= int(std_note_list[i])
        print("%d\t%s" % (window, json.dumps((midi_id, i))))

    count += 1
    if count % 1000 == 0:
        sys.stderr.write("mapper: " + str(count) + "\n")