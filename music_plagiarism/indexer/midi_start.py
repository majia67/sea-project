import subprocess
from common.inventory import IDX_SHARD_NUM, MIDI_SHARD_NUM
from indexer.inventory import midifile_path, midi_job_path

# Reformat
subprocess.call(["python", "-m", "indexer.midi_reformatter",
    "--new_folder=" + midifile_path,
    "--job_path=" + midi_job_path,
    "--num_partitions=" + str(MIDI_SHARD_NUM)])

# Index Building
subprocess.call(["python", "-m", "mapreduce.coordinator",
    "--mapper_path=indexer/midi_mapper.py",
    "--reducer_path=indexer/midi_reducer.py",
    "--job_path=" + midi_job_path,
    "--num_reducers=" + str(IDX_SHARD_NUM)])

# Move out files
subprocess.call(["python", "-m", "indexer.moveOutFiles"])
