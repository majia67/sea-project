import midi
import os
import argparse
import hashlib
import pickle
from indexer.inventory import note_name
from common.inventory import SLIDING_WINDOW_SIZE, IDX_SHARD_PATH, MIDI_SHARD_PATH, IDX_SHARD_NUM, MIDI_SHARD_NUM, MIDI_OCTAVE_SIZE, songlist_path

def getNoteOn(midi_file):
    pattern = midi.read_midifile(midi_file)
    return split_channels(pattern)

def split_channels(pattern):
    tracks = []
    info = []
    for track in pattern:
        channel = {}
        for event in track:
            if event.name == "Note On":
                if event.data[1] != 0:
                    if event.channel in channel:
                        channel[event.channel].append(event.data[0])
                    else:
                        channel[event.channel] = [event.data[0]]
        tracks.append(channel)
    for i, c in enumerate(tracks):
        for key, value in c.items():
            info.append({"track":i, "channel": key, "notes": value})
    return info

if __name__ == "__main__":
    argparser = argparse.ArgumentParser()
    argparser.add_argument("--new_folder", help = "Input newly added midi file folder name. The folder should be under midiFile directory.")
    argparser.add_argument("--job_path", help = "Map reduce job path")
    argparser.add_argument("--num_partitions", type=int, help = "number of mapreduce job partitions")
    args = argparser.parse_args()

    files = []
    for i in range(args.num_partitions):
        files.append(open(args.job_path+"/"+str(i)+".in", "w"))

    new_folder = args.new_folder

    songlist = open(songlist_path, 'r+')

    doc = {}
    song = {}
    error_message = []
    count = 0;

    # Get the last line of the songlist(song_id\tsong_title)
    last_line = ''
    for line in songlist:
        last_line = line

    # Compute the starting midi_id of this file. If the file is empty, assign midi_id to 0
    if last_line:
        midi_id = int(line.split('\t')[0]) + 1
    else:
        midi_id = 0

    # For every midi file in the newly added foler
    for path, subdirs, file in os.walk(new_folder):
        for file_name in file:

    #for file_name in os.listdir(new_folder_path):

            # If the file is not a midi file (not end with .mid)
            if file_name[-4:] != ".mid":
                continue
            try:
                count += 1;
                if count % 500 == 0:
                    print("Number of songs completed:"+str(count))
                pattern = midi.read_midifile(os.path.join(path, file_name))
            except Exception, e:
                error_message.append( "Error:" + str(e) + " " + os.path.join(path, file_name))
                continue

            # Split different channels
            info = split_channels(pattern)

            for i in info:
                note_seq = i["notes"]

                # Only consider channels with note sequence size larger than window size
                if len(note_seq) >= SLIDING_WINDOW_SIZE:
                    inverted_num = int(hashlib.md5(str(midi_id).encode('utf-8')).hexdigest()[:8], 16) % args.num_partitions

                    files[inverted_num].write("%d\t%s\n" % (midi_id, ' '.join([str(note) for note in note_seq])))

                    # Write midi id, note sequence and song title to the doc dictionary
                    doc[midi_id] = {"notes":note_seq, "title": file_name, "channel": i["channel"], "track": i["track"]}
                    song[midi_id] = file_name
                    midi_id += 1

    for midi_id in song:
        songlist.write(str(midi_id)+"\t"+song[midi_id]+"\n")

    for i in range(MIDI_SHARD_NUM):
        if not os.path.exists(MIDI_SHARD_PATH+"/"+str(i)):
            os.makedirs(MIDI_SHARD_PATH+"/"+str(i))

    for midi_id in doc:
        midi_shard = midi_id % MIDI_SHARD_NUM
        pickle.dump(doc[midi_id], open(MIDI_SHARD_PATH+"/"+str(midi_shard)+"/"+str(midi_id), "wb"))

    if len(error_message) != 0:
        print("There're "+str(len(error_message))+" songs skipped. The error meassage is as the following: ")
        for err in error_message:
            print(err)

    for file in files:
        file.close()







