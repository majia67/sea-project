'''Index server module'''
import pickle
import json
import logging
from tornado import web, httpclient
from common.inventory import IDX_SHARD_PATH

class IndexQueryHandler(web.RequestHandler):
    def initialize(self, shard_ix, cache):
        self._shard_ix = shard_ix
        self._idx_cache = cache

    def get(self):
        log = logging.getLogger(__name__)
        window = self.get_argument('q', None)
        log.info("Index server %d - Get query: %s", self._shard_ix, window)
        res = self._idx_cache.get(window)
        if res is not None:
            pass
#            log.info("Index server %d - Hit index cache: %s", self._shard_ix, window)
        else:
            try:
                file_path = '%s/%d/%s' % (IDX_SHARD_PATH, self._shard_ix, window)
                res = pickle.load(open(file_path, 'rb'))
                self._idx_cache.set(window, res)
#                log.info("Index server %d - Hit index: %s", self._shard_ix, window)
            except IOError:
                pass
                # Couldn't find the index file of the window
                # Could be a new window which has no match in the database
#                log.debug("Index server %d - Could not open index file: %s", self._shard_ix, file_path)

        self.finish(json.dumps({window: res}))
