'''Front end module'''
import urllib
from tornado import web, httpclient, gen
from search.inventory import BLAST_SERV_NUM, BLAST_SERV_URL
from common.inventory import MIDI_NOTE_NUMBER

class FrontEndHandler(web.RequestHandler):
    '''Front end handler

    Use Round-robin to decide which BLAST server is used for doing the search
    '''
    next_server = 0

    @gen.coroutine
    def get(self):
        query = self.get_argument('q', '')
        shift = int(self.get_argument('shift', 0))
        if shift != 0:
            # Shift notes
            note_list = query.split()
            for i, note in enumerate(note_list):
                note = int(note)
                if note + shift in range(0, MIDI_NOTE_NUMBER):
                    note_list[i] = str(note + shift)
                else:
                    print("error herere!!")
                    raise web.HTTPError(500)
            query = ' '.join(note_list)

        # Query BLAST server
        http = httpclient.AsyncHTTPClient(force_instance=True, defaults=dict(connect_timeout=120, request_timeout=120))
        server = BLAST_SERV_URL[FrontEndHandler.next_server]
        FrontEndHandler.next_server = (FrontEndHandler.next_server + 1) % BLAST_SERV_NUM
        query_string = urllib.urlencode({'q': query})
        response = yield http.fetch("http://%s/blast?%s" % (server, query_string))
        if response.error:
            raise web.HTTPError(500)

        # Return result
        self.finish(response.body)


class HTMLHandler(web.StaticFileHandler):
    '''Static HTML web page handler'''
    def parse_url_path(self, url_path):
        if not url_path or url_path.endswith('/'):
            url_path += 'index.html'
        return super(HTMLHandler, self).parse_url_path(url_path)
