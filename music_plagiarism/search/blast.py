"""Blast search module"""

import urllib
import json
import logging
from collections import defaultdict
from tornado import web, gen, httpclient
from common.inventory import IDX_SHARD_NUM, MIDI_SHARD_NUM, SLIDING_WINDOW_SIZE
from common.inventory import MIDI_NOTE_NUMBER, MIDI_OCTAVE_SIZE, MIDI_STD_NOTE_BITS
from search.inventory import IDX_SERV_URL, MIDI_SERV_URL, SCORE_INITIAL, SCORE_CUTOFF
from search.inventory import MAX_QUERY_RETURN, MAX_SUBQUERY_RETURN
from search.smith_waterman import score_function, score_window

WINDOW_FILTER = int('1' * (SLIDING_WINDOW_SIZE * MIDI_STD_NOTE_BITS), 2)

def get_sliding_windows(note_list):
    # Convert query notes into standard note list
    std_note_list = []
    for note in note_list:
        std_note = int(note) % MIDI_OCTAVE_SIZE
        std_note_list.append(std_note)

    # Generate first window
    window = 0
    for i in range(SLIDING_WINDOW_SIZE):
        window <<= MIDI_STD_NOTE_BITS
        window |= std_note_list[i]

    # Generate windows
    window_list = [window]
    for i in range(SLIDING_WINDOW_SIZE, len(std_note_list)):
        window <<= MIDI_STD_NOTE_BITS
        window &= WINDOW_FILTER
        window |= std_note_list[i]
        window_list.append(window)

    return window_list

class BlastSearchHandler(web.RequestHandler):
    """Blast search handler"""

    def initialize(self, shard_ix):
        self._shard_ix = shard_ix
        self._logger = logging.getLogger(__name__)

    @gen.coroutine
    def get(self):
        # Process query and generate sliding windows
        query_string = self.get_argument('q', '')
        self._logger.info('BLAST server %d gets query %s', self._shard_ix, query_string)
        query_notes = [int(note) for note in query_string.split()]
        if len(query_notes) < SLIDING_WINDOW_SIZE:
            return
        sliding_windows = get_sliding_windows(query_notes)
        sliding_windows_set = set(sliding_windows)

        # Query index and midi servers
        responses = yield self._get_index_server_futures(sliding_windows_set)
        index_list = self._get_index_list(responses, sliding_windows)
#        responses = yield self._get_index_server_futures(sliding_windows)
#        index_list = self._get_index_list(responses)
        index_agg = self._aggregate_index(index_list)

        responses = yield self._get_midi_server_futures(index_agg)
        midi_map = self._get_midi_map(responses)

        # Extend each window in the same midi file
        windows_ext = self._extend_windows(query_notes, index_agg, midi_map)

        # Sort the windows
        windows_list = self._sort_windows(windows_ext, midi_map)
        windows_result = windows_list[:MAX_QUERY_RETURN]

        #  Choose the top K scored windows and return complete result list
        rlt = {'results': windows_result, 'num_results': len(windows_result)}
        self.write(json.dumps(rlt))

    def _get_index_server_futures(self, sliding_windows_set):
        # Batch query index servers
        # Expected return format: JSON map, key=window, value={[(midi_id1, pos1), (midi_id2, pos2), ...]}
        http = httpclient.AsyncHTTPClient(max_clients=50)
        futures = []
        for window in sliding_windows_set:
            server = IDX_SERV_URL[window % IDX_SHARD_NUM]
            query_string = urllib.urlencode({'q': window})
            futures.append(http.fetch('http://%s/index?%s' % (server, query_string)))
        return futures

    def _get_index_list(self, responses, sliding_windows):
        # Decode responses and generate index list
        index_map = {}
        for res in responses:
            res_decode = json.loads(res.body.decode())
            key = res_decode.keys()[0]
            value = res_decode[key]
            index_map[int(key)] = value

        index_list = []
        for window in sliding_windows:
            index_list.append(index_map[window])
        return index_list

    # def _get_index_list(self, responses):
    #     index_list = []
    #     for res in responses:
    #         res_decode = json.loads(res.body.decode())
    #         key = res_decode.keys()[0]
    #         value = res_decode[key]
    #         index_list.append(value)
    #     return index_list

    def _get_midi_server_futures(self, index_agg):
        # Batch query midi servers to get the midi files
        # Expected return format: JSON map, key=midi_id, value={'title': midi song name, 'notes': [note list], other metadata...}
        max_query_per_request = 100
        http = httpclient.AsyncHTTPClient(max_clients=100)
        futures = []
        midi_list = [[] for i in range(MIDI_SHARD_NUM)]
        for midi_id in index_agg.keys():
            shard_ix = midi_id % MIDI_SHARD_NUM
            midi_list[shard_ix].append(midi_id)
#        self._logger.debug("midi_list = %s", str(midi_list))
        for shard_ix, mlist in enumerate(midi_list):
#            self._logger.debug("mlist = %s", str(mlist))
            server = MIDI_SERV_URL[shard_ix]
            num = len(mlist)
            if num % max_query_per_request == 0:
                divide = int(num / max_query_per_request)
            else:
                divide = int(num / max_query_per_request) + 1
            for i in range(0, divide):
                st = i * max_query_per_request
                ed = (i + 1) * max_query_per_request
                query = ' '.join([str(item) for item in mlist[st : ed]])
                query_string = urllib.urlencode({'q': query})
                futures.append(http.fetch('http://%s/midi?%s' % (server, query_string)))
        return futures

    def _get_midi_map(self, responses):
        # Decode responses and generate midi map
        midi_map = {}
        for res in responses:
            res_decode = json.loads(res.body.decode())
            for midi_id, midi_info in res_decode.items():
                midi_map[int(midi_id)] = midi_info
        return midi_map

    def _aggregate_index(self, index_list):
        # Aggregate index result by midi_id
        index_agg = defaultdict(list)
        for query_pos, idx_pos_list in enumerate(index_list):
            if idx_pos_list is None:
                continue
            for midi_id, idx_pos in idx_pos_list:
                index_agg[midi_id].append((query_pos, idx_pos, SLIDING_WINDOW_SIZE))
        return index_agg

    def _extend_windows(self, query_notes, index_agg, midi_map):
        windows_ext = defaultdict(list)
        for midi_id in index_agg.keys():
            for index in index_agg[midi_id]:
                is_included = False
                if midi_id in windows_ext:
                    query_pos, idx_pos, _ = index
                    q_pos, m_pos, len_ext = windows_ext[midi_id][-1]['pos']
                    if (idx_pos in range(m_pos, m_pos + len_ext)) and (
                            query_pos in range(q_pos, q_pos + len_ext)):
                        # This window is included in a previous extended window
                        is_included = True
                    # for i in range(len(windows_ext[midi_id])):
                    #     q_pos, m_pos, len_ext = windows_ext[midi_id][i]['pos']
                    #     if (idx_pos in range(m_pos, m_pos + len_ext)) and (
                    #             query_pos in range(q_pos, q_pos + len_ext)):
                    #         # This window is included in a previous extended window
                    #         is_included = True
                    #         break
                if not is_included:
                    rlt = self._extend_window(query_notes, midi_map[midi_id]['notes'], index)
                    windows_ext[midi_id].append(rlt)
        return windows_ext

    def _extend_window(self, query_notes, midi_notes, index):
        q_pos, m_pos, len_ext = index

        # Extend to head
        score = score_window(query_notes[q_pos : q_pos + len_ext], midi_notes[m_pos : m_pos + len_ext])
        max_score = score
        max_head_ext = 0
        for i in range(1, min(q_pos, m_pos)):
            score += score_function(query_notes[q_pos - i], midi_notes[m_pos - i])
            if score > max_score:
                max_score = score
                max_head_ext = i
            elif score < (max_score * SCORE_CUTOFF):
                break

        # Extend to tail
        score = max_score
        q_end = q_pos + len_ext - 1
        m_end = m_pos + len_ext - 1
        max_tail_ext = 0
        ext_range = min(len(query_notes) - q_end, len(midi_notes) - m_end)
        for i in range(1, ext_range):
            score += score_function(query_notes[q_end + i], midi_notes[m_end + i])
            if score > max_score:
                max_score = score
                max_tail_ext = i
            elif score < (max_score * SCORE_CUTOFF):
                break

        # Return result
        q_pos -= max_head_ext
        m_pos -= max_head_ext
        len_ext += max_head_ext + max_tail_ext
        rlt = {'pos': (q_pos, m_pos, len_ext), 'score': max_score,
               'query_notes': query_notes[q_pos : q_pos + len_ext],
               'midi_notes': midi_notes[m_pos : m_pos + len_ext]}
        return rlt

    def _sort_windows(self, windows_ext, midi_map):
        windows_list = []
        for midi_id, windows in windows_ext.items():
            # Only keep the top K results for each song
            windows.sort(key=lambda w: w['score'], reverse=True)
            windows = windows[:MAX_SUBQUERY_RETURN]
            midi_score = sum([win['score'] for win in windows])
            windows_list.append({'title': midi_map[midi_id]['title'],
                                 'channel': midi_map[midi_id]['channel'],
                                 'midi_score': midi_score,
                                 'windows': windows})
        windows_list.sort(key=lambda w: w['midi_score'], reverse=True)
        return windows_list
