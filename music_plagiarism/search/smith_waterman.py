
from search.inventory import SCORE_MATCH, SCORE_MISMATCH, SCORE_SN_MATCH
from common.inventory import MIDI_OCTAVE_SIZE

def smith_waterman(query_notes, midi_notes):
    m, n = len(query_notes), len(midi_notes)
    mat = [[0] * (m + 1) for i in range(n + 1)]
    maxi, maxj = 0, 0
    for i in range(1, m + 1):
        for j in range(1, n + 1):
            mat[i][j] = max(
                0,
                mat[i-1][j-1] + score(query_notes[i], midi_notes[j]),
                max(mat[i-1][j], mat[i][j-1] + SCORE_GAP))
            if mat[i][j] > mat[maxi][maxj]:
                maxi, maxj = i, j
    # TODO: Traceback
    return mat[maxi][maxj]

def score_function(note1, note2):
    if note1 == note2:
        return SCORE_MATCH
    elif note1 % MIDI_OCTAVE_SIZE == note2 % MIDI_OCTAVE_SIZE:
        return SCORE_SN_MATCH
    else:
        return SCORE_MISMATCH

def score_window(query, midi):
    score = 0
    for i in range(len(query)):
        score += score_function(query[i], midi[i])
    return score
