var searchApp = angular.module('searchApp', ['ngSanitize']).config(
    function($locationProvider) {
        $locationProvider.html5Mode(true);
    });

searchApp.controller('mainController', function ($scope, $http, $location) {
    $scope.formData = {};
    lastQuery = "";
    $scope.search = function() {
        if ($scope.formData.query) {
            searchArgs = {}
            searchArgs['q'] = $scope.formData.query;
            if ($scope.formData.shift) {
                searchArgs['shift'] = $scope.formData.shift;
            }
            $location.search(searchArgs);
        }
    };

    $scope.fetchRecords = function() {
        var start = new Date().getTime();
        var queryString = '/search?q=' + encodeURIComponent($scope.formData.query);
        if ($scope.formData.shift) {
            queryString += '&shift=' + $scope.formData.shift;
        }
        if (queryString !== lastQuery) {
            lastQuery = queryString;
            console.log('queryString=' + queryString);
            $http.get(queryString).success(function(data) {
                $scope.delay = (new Date().getTime() - start) / 1000.0;
                $scope.totalItems = data.num_results;
                $scope.errorText = data.error;
                $scope.results = data.results;
            }).error(function(data) {
                $scope.results = [];
                $scope.totalItems = 0;
                $scope.errorText = data;
            });
        }
    };

    $scope.$on('$locationChangeSuccess', function(event){
        if ($location.search().q) {
            $scope.formData.query = $location.search().q;
            $scope.formData.shift = $location.search().shift;
//            $scope.activeQuery = $scope.formData.query;
            $scope.fetchRecords();
//        } else {
//            $scope.activeQuery = false;
        }
    });
});
