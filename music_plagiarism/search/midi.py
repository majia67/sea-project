"""MIDI store server && similarity test"""
import json
import pickle
import logging
from tornado import web, httpclient
from common.inventory import MIDI_SHARD_PATH

class MIDIQueryHandler(web.RequestHandler):
    def initialize(self, shard_ix, cache):
        self._shard_ix = shard_ix
        self._midi_cache = cache

    def get(self):
        log = logging.getLogger(__name__)
        query = self.get_argument('q', None)
        log.info("MIDI server %d - Get query: %s", self._shard_ix, query)
        midi_id_list = query.strip().split()
        result = {}
        for midi_id in midi_id_list:
            res = self._midi_cache.get(midi_id)
            if res is not None:
#                log.info("MIDI server %d - Hit midi cache: %s", self._shard_ix, midi_id)
                pass
            else:
                try:
                    file_path = '%s/%d/%s' % (MIDI_SHARD_PATH, self._shard_ix, midi_id)
                    res = pickle.load(open(file_path, 'rb'))
                    self._midi_cache.set(midi_id, res)
#                    log.info("MIDI server %d - Hit id: %s", self._shard_ix, midi_id)
                except IOError:
                    pass
#                    log.info("MIDI server %d - Could not open midi file: %s", self._shard_ix, file_path)
                    raise
            result[midi_id] = res
        self.finish(json.dumps(result))
