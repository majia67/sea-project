'''Search Engine start module'''
import logging
from tornado import ioloop, web, process, httpserver, netutil
from search import frontend, blast, index, midi
from common.inventory import IDX_SHARD_NUM, MIDI_SHARD_NUM
from search.inventory import BASE_PORT, BLAST_SERV_NUM, WEBAPP_PATH
from search.inventory import IDX_CACHE_CAPACITY, MIDI_CACHE_CAPACITY
from lru_cache import LRUCache

SETTINGS = {'static_path': WEBAPP_PATH}

def main():
    log = logging.getLogger(__name__)
    num_procs = BLAST_SERV_NUM + IDX_SHARD_NUM + MIDI_SHARD_NUM + 1
    task_id = process.fork_processes(num_procs, max_restarts=0)
    port = BASE_PORT + task_id
    if task_id == 0:
        app = httpserver.HTTPServer(web.Application([
            (r'/search', frontend.FrontEndHandler),
            (r'/(.*)', frontend.HTMLHandler, dict(path=SETTINGS['static_path']))
            ], **SETTINGS))
        log.info('Front end is listening on %d', port)
    else:
        if task_id <= BLAST_SERV_NUM:
            shard_ix = task_id - 1
            app = httpserver.HTTPServer(web.Application([
                (r'/blast', blast.BlastSearchHandler, dict(shard_ix=shard_ix))]))
            log.info('BLAST server %d is listening on %d', shard_ix, port)
        elif task_id <= BLAST_SERV_NUM + IDX_SHARD_NUM:
            shard_ix = task_id - BLAST_SERV_NUM - 1
            idx_cache = LRUCache(IDX_CACHE_CAPACITY)
            app = httpserver.HTTPServer(web.Application([
                (r'/index', index.IndexQueryHandler, dict(shard_ix=shard_ix, cache=idx_cache))]))
            log.info('Index server %d is listening on %d', shard_ix, port)
        else:
            shard_ix = task_id - BLAST_SERV_NUM - IDX_SHARD_NUM - 1
            midi_cache = LRUCache(MIDI_CACHE_CAPACITY)
            app = httpserver.HTTPServer(web.Application([
                (r'/midi', midi.MIDIQueryHandler, dict(shard_ix=shard_ix, cache=midi_cache))]))
            log.info('MIDI server %d is listening on %d', shard_ix, port)
    app.add_sockets(netutil.bind_sockets(port))
    ioloop.IOLoop.current().start()

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s - %(asctime)s - %(message)s', level=logging.DEBUG)
    main()
