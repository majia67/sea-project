import sys
import fnmatch
import os
import argparse
from tornado.escape import json_decode
from tornado import gen, httpclient
from tornado.ioloop import IOLoop
from .inventory import servers, W

argparser = argparse.ArgumentParser()
argparser.add_argument("--mapper_path", help="")
argparser.add_argument("--reducer_path", help="")
argparser.add_argument("--job_path", help="")
argparser.add_argument("--num_reducers", type=int, help="")
args = argparser.parse_args()

mapper_path = args.mapper_path
reducer_path = args.reducer_path
job_path = args.job_path
num_reducers = args.num_reducers

print(mapper_path, reducer_path, job_path, num_reducers)

@gen.coroutine
def main():
    http = httpclient.AsyncHTTPClient(force_instance=True, defaults=dict(connect_timeout=3600, request_timeout=3600))
    output = []
    i = 0
    for file in os.listdir(job_path):
        if fnmatch.fnmatch(file, '*.in'):
            url = "http://" + servers[i % W] + '/map?mapper_path=' + mapper_path + '&input_file=' + job_path + "/" + file + "&num_reducers=" + str(num_reducers)
            output.append(http.fetch(url))
            i += 1
    response = yield output

    map_task_ids = []
    for r in response:
         map_task_ids.append(json_decode(r.body)['map_task_id'])
    reducer_output = []
    for i in range(num_reducers):
        url = "http://" + servers[i % W] + '/reduce?reducer_ix=' + str(i) + '&reducer_path=' + reducer_path + '&map_task_ids=' + ",".join(map_task_ids) + '&job_path=' + job_path
        reducer_output.append(http.fetch(url))
    reducer_response = yield reducer_output

if __name__ == "__main__":
    IOLoop.current().run_sync(main)


