import tornado.ioloop
import tornado.web
import hashlib, getpass
import time
from tornado.escape import json_decode
from tornado import gen, httpclient, httpserver, process, netutil
import json, urllib, subprocess, pickle
from .inventory import W, servers, ports, BASE_PORT

mapper_dict = {}

class mapHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        mapper_path = self.get_argument('mapper_path')
        input_file = self.get_argument('input_file')
        num_reducers = int(self.get_argument('num_reducers'))
        mapper_rlt_list = []
        for i in range(num_reducers):
            mapper_rlt_list.append([])

        input_f = open(input_file, 'r')
        p = subprocess.Popen(mapper_path, stdin=input_f, stdout=subprocess.PIPE)
        (out, _) = p.communicate()
        input_f.close()
        out = out.decode()
        pair_list = out.strip('\n').split('\n')

        for pair in pair_list:
            pair = pair.split('\t')
            i = int(hashlib.md5(pair[0].encode('utf-8')).hexdigest()[:8], 16)%num_reducers
            mapper_rlt_list[i].append(pair)

        map_task_id = hashlib.md5(str(time.time()).encode('utf-8')).hexdigest()
        mapper_dict[map_task_id] = mapper_rlt_list
        self.finish({"status": "success", "map_task_id": map_task_id})

class retrieveHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        reducer_ix = self.get_argument('reducer_ix')
        map_task_id = self.get_argument('map_task_id')
        self.finish(json.dumps(mapper_dict[map_task_id][int(reducer_ix)]))

class reduceHandler(tornado.web.RequestHandler):
    @gen.coroutine
    def get(self):
        reducer_ix = self.get_argument('reducer_ix')
        reducer_path = self.get_argument('reducer_path')
        map_task_ids = self.get_argument('map_task_ids')
        job_path = self.get_argument('job_path')
        #servers = ["linserv2.cims.nyu.edu:34514", "linserv2.cims.nyu.edu:34515"]
        map_task_ids = map_task_ids.split(',')
        num_mappers = len(map_task_ids)

        http = httpclient.AsyncHTTPClient(force_instance=True, defaults=dict(connect_timeout=3600, request_timeout=3600, max_buffer_size=100*1024*1024*1024))
        futures = []
        for i in range(num_mappers):
            server = servers[i % len(servers)]
            params = urllib.urlencode({'reducer_ix': reducer_ix,
                                             'map_task_id': map_task_ids[i]})
            url = "http://%s/retrieve_map_output?%s" % (server, params)
            print("Fetching", url)
            futures.append(http.fetch(url))
        responses = yield futures


        kv_pairs = []
        for r in responses:
            #print(json.loads(r.body.decode()))
            kv_pairs.extend(json.loads(r.body.decode()))
        kv_pairs.sort(key=lambda x: x[0])

        kv_string = "\n".join([pair[0] + "\t" + pair[1] for pair in kv_pairs])

        out = open(job_path + "/" + reducer_ix + '.out', 'w')
        p = subprocess.Popen(reducer_path, stdin=subprocess.PIPE, stdout=out)
        p.communicate(kv_string.encode())
        out.close()

        self.finish({"status": "success"})

def worker_make_app(port):
    app = httpserver.HTTPServer(tornado.web.Application([
        (r"/retrieve_map_output", retrieveHandler),
        (r"/reduce", reduceHandler),
        (r"/map", mapHandler)
    ]))
    app.add_sockets(netutil.bind_sockets(port))
    return app

if __name__ == "__main__":
    ##the following two lines are taken from solution of homework1
    task_id = process.fork_processes(W, max_restarts = 0)
    worker_apps = worker_make_app(BASE_PORT + task_id)
    print(servers[task_id])
    tornado.ioloop.IOLoop.current().start()


